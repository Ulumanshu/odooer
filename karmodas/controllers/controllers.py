# -*- coding: utf-8 -*-
from odoo import http


class Karmodas(http.Controller):

    @http.route('/karmodas/karmodas/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/karmodas/karmodas/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('karmodas.listing', {
            'root': '/karmodas/karmodas',
            'objects': http.request.env['karmodas.karmodas'].search([]),
        })

    @http.route('/karmodas/karmodas/objects/<model("karmodas.karmodas"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('karmodas.object', {
            'object': obj
        })
