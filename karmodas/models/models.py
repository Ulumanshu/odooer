# -*- coding: utf-8 -*-

from odoo import models, fields, api


class karmodas(models.Model):
    _name = 'karmodas.karmodas'
    # _inherit = 'res.partner'

    @api.model
    def create(self, vals, context=None):
        if context is None:
            context = {}
        result = super(karmodas, self).create(vals)
        result.name = result.name[:-1]

        return result

    @api.model
    def search(self, args, **kwargs):
        print(args)
        result = super(karmodas,self).search(args, **kwargs)

        return result

    @api.multi
    def write(self, vals):
        if vals.get('name') and not self._context.get('no_create'):
            self = self.with_context(no_create=True)
            partners = self.search([], limit=2)
            for partner in partners:
                partner.name = vals['name']
        result = super(karmodas, self).write(vals)
        return result

    @api.multi
    @api.depends("value", "value2")
    def _calculate_field(self):
        for record in self:
            record.value3 = record.value + record.value2


    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    value3 = fields.Float(compute="_calculate_field", store=True)
    description = fields.Text()

    @api.one
    @api.depends('value')
    def _value_pc(self):
        self.value2 = float(self.value) / 100


class ResPartner(models.Model):
    _inherit = "res.partner"

    kar_id = fields.Many2one("karmodas.karmodas")
