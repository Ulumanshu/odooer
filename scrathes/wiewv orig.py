<!-- explicit list view definition -->
    <!--
    <record model="ir.ui.view" id="karmodas.list">
      <field name="name">Karmodas list</field>
      <field name="model">karmodas.karmodas</field>
      <field name="arch" type="xml">
        <tree>
          <field name="name"/>
          <field name="value"/>
          <field name="value2"/>
        </tree>
      </field>
    </record>
    -->

    <!-- actions opening views on models -->
    <!--
    <record model="ir.actions.act_window" id="karmodas.action_window">
      <field name="name">Karmodas window</field>
      <field name="res_model">karmodas.karmodas</field>
      <field name="view_mode">tree,form</field>
    </record>
    -->

    <!-- server action to the one above -->
    <!--
    <record model="ir.actions.server" id="karmodas.action_server">
      <field name="name">Karmodas server</field>
      <field name="model_id" ref="model_karmodas_karmodas"/>
      <field name="state">code</field>
      <field name="code">
        action = {
          "type": "ir.actions.act_window",
          "view_mode": "tree,form",
          "res_model": self._name,
        }
      </field>
    </record>
    -->

    <!-- Top menu item -->
    <!--
    <menuitem name="Karmodas" id="karmodas.menu_root"/>
    -->
    <!-- menu categories -->
    <!--
    <menuitem name="Menu 1" id="karmodas.menu_1" parent="karmodas.menu_root"/>
    <menuitem name="Menu 2" id="karmodas.menu_2" parent="karmodas.menu_root"/>
    -->
    <!-- actions -->
    <!--
    <menuitem name="List" id="karmodas.menu_1_list" parent="karmodas.menu_1"
              action="karmodas.action_window"/>
    <menuitem name="Server to list" id="karmodas" parent="karmodas.menu_2"
              action="karmodas.action_server"/>
    -->